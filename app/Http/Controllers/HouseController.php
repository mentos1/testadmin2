<?php

namespace App\Http\Controllers;

use App\Repositories\House\HouseRepository;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    private $houseRepository;

    public function __construct(HouseRepository $houseRepository)
    {
        $this->houseRepository = $houseRepository;
    }

    public function getPageAdding()
    {
        return view('index', $this->houseRepository->getPageAdding());
    }

    public function save(Request $request)
    {
        $this->houseRepository->save($request);

        return redirect()->route('getPageAdding');
    }

    public function getPageSearch(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('search', $this->houseRepository->getPageSearch($request));
        } elseif ($request->isMethod('post')) {
            return view('search', $this->houseRepository->find($request));
        }
    }

}
