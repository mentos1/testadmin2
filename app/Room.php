<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use CrudTrait;
    protected $table = 'rooms';

    public function rooms() {
        return $this->belongsTo('App\House');
    }
}
