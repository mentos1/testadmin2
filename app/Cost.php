<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    use CrudTrait;

    protected $table = 'costs';
    protected $fillable = ['cost'];
    public $timestamps = false;

    public function costs() {
        return $this->belongsTo('App\House');
    }
}
