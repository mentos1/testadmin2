<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->integer('id_city')->unsigned()->default(1);
            $table->foreign('id_city')->references('id')->on('cities');

            $table->integer('id_rend')->unsigned()->default(1);
            $table->foreign('id_rend')->references('id')->on('rends');

            $table->integer('id_cost')->unsigned()->default(1);
            $table->foreign('id_cost')->references('id')->on('costs');

            $table->integer('id_square')->unsigned()->default(1);
            $table->foreign('id_square')->references('id')->on('squares');

            $table->integer('id_room')->unsigned()->default(1);
            $table->foreign('id_room')->references('id')->on('rooms');

            $table->integer('id_floor')->unsigned()->default(1);
            $table->foreign('id_floor')->references('id')->on('floors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
