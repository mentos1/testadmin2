<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cities')->insert([
            'city' => 'Винница'
        ]);
        DB::table('cities')->insert([
            'city' => 'Луцк'
        ]);
        DB::table('cities')->insert([
            'city' => 'Днепр'
        ]);
        DB::table('cities')->insert([
            'city' => 'Донецк'
        ]);
        DB::table('cities')->insert([
            'city' => 'Житомир'
        ]);
        DB::table('cities')->insert([
            'city' => 'Ужгород'
        ]);
        DB::table('cities')->insert([
            'city' => 'Запорожье'
        ]);
        DB::table('cities')->insert([
            'city' => 'Ивано-Франковск'
        ]);
        DB::table('cities')->insert([
            'city' => 'Киев'
        ]);
        DB::table('cities')->insert([
            'city' => 'Кропивницкий'
        ]);
        DB::table('cities')->insert([
            'city' => 'Луганск'
        ]);
        DB::table('cities')->insert([
            'city' => 'Львов'
        ]);
        DB::table('cities')->insert([
            'city' => 'Николаев'
        ]);
        DB::table('cities')->insert([
            'city' => 'Одесса'
        ]);
        DB::table('cities')->insert([
            'city' => 'Полтава'
        ]);
        DB::table('cities')->insert([
            'city' => 'Ровно'
        ]);
        DB::table('cities')->insert([
            'city' => 'Сумы'
        ]);
        DB::table('cities')->insert([
            'city' => 'Тернополь'
        ]);
        DB::table('cities')->insert([
            'city' => 'Харьков'
        ]);
        DB::table('cities')->insert([
            'city' => 'Херсон'
        ]);
        DB::table('cities')->insert([
            'city' => 'Хмельницкий'
        ]);
        DB::table('cities')->insert([
            'city' => 'Черкассы'
        ]);
        DB::table('cities')->insert([
            'city' => 'Чернигов'
        ]);
        DB::table('cities')->insert([
            'city' => 'Черновцы'
        ]);
    }

}
