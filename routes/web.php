<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HouseController@getPageAdding')->name('getPageAdding');
Route::post('/save', 'HouseController@save')->name('save');
Route::match(['get', 'post'], '/search', 'HouseController@getPageSearch')->name('getPageSearch');
